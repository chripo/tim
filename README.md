TIM - Tasks IMproved
====================

TIM stands for Tasks IMproved and provides a offline application that automatically syncs your entries as soon as you get online.

Features
--------

* page even available when you are offline
* multiple groups to making collaboration with different people easier
* new entries are added via a simple form or over a syntax as a oneliner
* [...]

Used software
-------------

TIM uses the following projects or parts of it's code:

* [Picnic CSS](<http://picnicss.com/>),
  License: MIT
* [Mithril](<http://lhorie.github.io/mithril/>),
  License: MIT

Font license info
-----------------

## Web Symbols

   Copyright (c) 2011 by Just Be Nice studio. All rights reserved.

   Author:    Just Be Nice studio
   License:   SIL (http://scripts.sil.org/OFL)
   Homepage:  http://www.justbenicestudio.com/


## Font Awesome

   Copyright (C) 2012 by Dave Gandy

   Author:    Dave Gandy
   License:   SIL ()
   Homepage:  http://fortawesome.github.com/Font-Awesome/


## Fontelico

   Copyright (C) 2012 by Fontello project

   Author:    Crowdsourced, for Fontello project
   License:   SIL (http://scripts.sil.org/OFL)
   Homepage:  http://fontello.com


## Entypo

   Copyright (C) 2012 by Daniel Bruce

   Author:    Daniel Bruce
   License:   SIL (http://scripts.sil.org/OFL)
   Homepage:  http://www.entypo.com
